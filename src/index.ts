import { SQSEvent } from "aws-lambda";
import { logger } from "@nhsbsa/hrt-ppc-npm-logging";
import { processRecord } from "./process-sqs-event-record";

export async function handler(event: SQSEvent) {
  try {
    logger.info("hrt-ppc-notification-outbound-lambda");
    const { Records: records } = event;

    if (records.length !== 1) {
      logger.error(`Expected only 1 record but found ${records.length}`);
      throw new Error(`Expected only 1 record but found ${records.length}`);
    }

    await Promise.all(records.map((record) => processRecord(record)));
    return "Processing finished";
  } catch (err) {
    const message = err;
    logger.error(message);

    throw err;
  }
}
