import { getCertificateById } from "../certificate";
import { CertificateApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { mockCertificateRecord } from "../../__mocks__/mockData";

let certificateApiSpy: jest.SpyInstance;
const certificateId = "certificateId";
const headers = {
  header: "value",
};

beforeEach(() => {
  certificateApiSpy = jest
    .spyOn(CertificateApi.prototype, "makeRequest")
    .mockResolvedValue({});
});
afterEach(() => {
  jest.resetAllMocks();
});

describe("certificate", () => {
  describe("getCertificateById()", () => {
    it("should return the response successfully", async () => {
      certificateApiSpy.mockResolvedValue(mockCertificateRecord);
      const response = await getCertificateById(certificateId, headers);
      expect(response).toBe(mockCertificateRecord);
    });

    it("should call the certificate api with correct parameters", async () => {
      await getCertificateById(certificateId, headers);
      expect(certificateApiSpy).toHaveBeenCalledTimes(1);
      expect(certificateApiSpy.mock.calls).toMatchInlineSnapshot(`
        [
          [
            {
              "headers": {
                "header": "value",
              },
              "method": "GET",
              "responseType": "json",
              "url": "/v1/certificates/${certificateId}",
            },
          ],
        ]
      `);
    });
  });
});
