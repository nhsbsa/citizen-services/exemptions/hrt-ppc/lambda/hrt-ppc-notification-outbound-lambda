import { getCitizenById } from "../citizen";
import { CitizenApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { mockCitizenRecord } from "../../__mocks__/mockData";

let citizenApiSpy: jest.SpyInstance;
const citizenId = "citizenId";
const headers = {
  header: "value",
};

beforeEach(() => {
  citizenApiSpy = jest
    .spyOn(CitizenApi.prototype, "makeRequest")
    .mockResolvedValue({});
});
afterEach(() => {
  jest.resetAllMocks();
});

describe("citizen", () => {
  describe("getCitizenById()", () => {
    it("should return the response successfully", async () => {
      citizenApiSpy.mockResolvedValue(mockCitizenRecord);
      const response = await getCitizenById(citizenId, headers);
      expect(response).toBe(mockCitizenRecord);
    });

    it("should call the citizen api with correct parameters", async () => {
      await getCitizenById(citizenId, headers);
      expect(citizenApiSpy).toHaveBeenCalledTimes(1);
      expect(citizenApiSpy.mock.calls).toMatchInlineSnapshot(`
        [
          [
            {
              "headers": {
                "header": "value",
              },
              "method": "GET",
              "responseType": "json",
              "url": "/v1/citizens/${citizenId}",
            },
          ],
        ]
      `);
    });
  });
});
