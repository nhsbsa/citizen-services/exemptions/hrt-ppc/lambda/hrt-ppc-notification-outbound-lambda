// eslint-disable-next-line @typescript-eslint/no-var-requires
const NotifyClient = require("notifications-node-client").NotifyClient;

const sendEmailMock = jest.fn();
jest.mock("notifications-node-client", () => {
  return {
    NotifyClient: jest.fn(),
  };
});
const getSecretValueMock = jest
  .fn()
  .mockResolvedValueOnce("test-key")
  .mockResolvedValueOnce("templateId")
  .mockResolvedValueOnce("test-key")
  .mockResolvedValueOnce("templateId")
  .mockResolvedValueOnce("test-key")
  .mockResolvedValueOnce("templateId")
  .mockResolvedValueOnce("test-key")
  .mockResolvedValueOnce("templateId")
  .mockResolvedValueOnce("test-key")
  .mockResolvedValueOnce("templateId")
  .mockResolvedValueOnce("test-key")
  .mockResolvedValueOnce("templateId")
  .mockResolvedValueOnce("test-key")
  .mockResolvedValueOnce("templateId");

jest.mock("@nhsbsa/health-charge-exemption-npm-utils-rest", () => ({
  EventType: {
    issueCertificate: "issue-certificate",
    reissueCertificate: "reissue-certificate",
    renewal: "renewal",
  },
  getSecretValue: getSecretValueMock,
}));

import { triggerEmailNotification } from "../email-notify";
import { EventType } from "@nhsbsa/health-charge-exemption-npm-utils-rest";

let personalisation, emailAddress;
beforeEach(() => {
  process.env.govNotifyApiKey = "test-key";
  emailAddress = "test@test.com";
  personalisation = {
    certReference: "testReference",
    endDate: "01 January 2024",
    fullName: "Test FullName",
    startDate: "01 January 2023",
  };

  NotifyClient.mockImplementation(() => ({ sendEmail: sendEmailMock }));
  sendEmailMock.mockResolvedValue({ id: "email-sent-id" });
});

describe("emailNotify", () => {
  describe("triggerEmailNotification()", () => {
    it.each([
      [EventType.issueCertificate, "GOV_NOTIFY_EMAIL_ISSUE_TEMPLATE_ID"],
      [EventType.reissueCertificate, "GOV_NOTIFY_EMAIL_REISSUE_TEMPLATE_ID"],
      [EventType.renewal, "GOV_NOTIFY_EMAIL_RENEWAL_TEMPLATE_ID"],
    ])(
      "should call the service with the apiKey and correct template key when event type is %p",
      async (eventType, templateId) => {
        await triggerEmailNotification(
          personalisation,
          emailAddress,
          eventType,
        );
        expect(getSecretValueMock).toHaveBeenCalledTimes(2);
        expect(getSecretValueMock).toBeCalledWith("GOV_NOTIFY_API_KEY");
        expect(getSecretValueMock).toBeCalledWith(templateId);
        expect(NotifyClient).toHaveBeenCalledTimes(1);
        expect(NotifyClient).toHaveBeenCalledWith("test-key");
      },
    );

    it.each([
      [EventType.issueCertificate, "GOV_NOTIFY_EMAIL_ISSUE_TEMPLATE_ID"],
      [EventType.reissueCertificate, "GOV_NOTIFY_EMAIL_REISSUE_TEMPLATE_ID"],
      [EventType.renewal, "GOV_NOTIFY_EMAIL_RENEWAL_TEMPLATE_ID"],
    ])(
      "should call the govNotify with the correct parameters",
      async (eventType, templateId) => {
        await triggerEmailNotification(
          personalisation,
          emailAddress,
          eventType,
        );
        expect(getSecretValueMock).toHaveBeenCalledTimes(2);
        expect(getSecretValueMock).toBeCalledWith("GOV_NOTIFY_API_KEY");
        expect(getSecretValueMock).toBeCalledWith(templateId);
        expect(sendEmailMock).toHaveBeenCalledWith(
          "templateId",
          "test@test.com",
          {
            personalisation: {
              certReference: "testReference",
              endDate: "01 January 2024",
              fullName: "Test FullName",
              startDate: "01 January 2023",
            },
            reference: "testReference",
          },
        );
      },
    );

    it.each([
      [EventType.issueCertificate, "GOV_NOTIFY_EMAIL_ISSUE_TEMPLATE_ID"],
      [EventType.reissueCertificate, "GOV_NOTIFY_EMAIL_REISSUE_TEMPLATE_ID"],
      [EventType.renewal, "GOV_NOTIFY_EMAIL_RENEWAL_TEMPLATE_ID"],
    ])(
      "should return the result of the govNotify service",
      async (eventType, templateId) => {
        const response = await triggerEmailNotification(
          personalisation,
          emailAddress,
          eventType,
        );
        expect(getSecretValueMock).toHaveBeenCalledTimes(2);
        expect(getSecretValueMock).toBeCalledWith("GOV_NOTIFY_API_KEY");
        expect(getSecretValueMock).toBeCalledWith(templateId);
        expect(response).toEqual({ id: "email-sent-id" });
      },
    );
  });
});
