const getSecretValueMock = jest.fn().mockResolvedValue("xeroxNotifyTemplateId");
jest.mock("@nhsbsa/health-charge-exemption-npm-utils-rest", () => ({
  ...jest.requireActual("@nhsbsa/health-charge-exemption-npm-utils-rest"),
  getSecretValue: getSecretValueMock,
}));
import { sendLetterXeroxNotification } from "../xerox-notification";
import { XeroxNotificationApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { mockXeroxNotificationRecord } from "../../__mocks__/mockData";

let xeroxNotificationApiSpy: jest.SpyInstance;
const personalisation = {
  addressLine1: "address one",
  addressLine2: "address two",
  addressLine3: "address three",
  certificateReference: "Ref",
  certificateStartDate: "11 November 2022",
  certificateEndDate: "10 November 2023",
  firstName: "first",
  lastName: "last",
};
const correlationId = "corId";
const reference = "ref";

beforeEach(() => {
  xeroxNotificationApiSpy = jest
    .spyOn(XeroxNotificationApi.prototype, "makeRequest")
    .mockResolvedValue({});
});

describe("xeroxNotification", () => {
  describe("sendLetterXeroxNotification()", () => {
    it("should return the response successfully", async () => {
      xeroxNotificationApiSpy.mockResolvedValue(mockXeroxNotificationRecord);
      const response = await sendLetterXeroxNotification(
        correlationId,
        reference,
        personalisation,
      );
      expect(response).toBe(mockXeroxNotificationRecord);
      expect(getSecretValueMock).toHaveBeenCalledTimes(1);
      expect(getSecretValueMock).toBeCalledWith(
        "XEROX_NOTIFICATION_TEMPLATE_ID",
      );
    });

    it("should call the xerox notification api", async () => {
      await sendLetterXeroxNotification(
        correlationId,
        reference,
        personalisation,
      );
      expect(getSecretValueMock).toHaveBeenCalledTimes(1);
      expect(getSecretValueMock).toBeCalledWith(
        "XEROX_NOTIFICATION_TEMPLATE_ID",
      );
      expect(xeroxNotificationApiSpy).toHaveBeenCalledTimes(1);
      expect(xeroxNotificationApiSpy.mock.calls[0]).toMatchInlineSnapshot(`
        [
          {
            "data": {
              "personalisation": {
                "addressLine1": "address one",
                "addressLine2": "address two",
                "addressLine3": "address three",
                "certificateEndDate": "10 November 2023",
                "certificateReference": "Ref",
                "certificateStartDate": "11 November 2022",
                "firstName": "first",
                "lastName": "last",
              },
              "reference": "ref",
              "templateId": "xeroxNotifyTemplateId",
            },
            "headers": {
              "channel": "BATCH",
              "correlation-id": "corId",
              "service-name": "HRT_PPC",
              "user-id": "SEND_NOTIFY_EVENT",
              "x-api-key": "XEROX_PUBLIC_KEY",
            },
            "method": "POST",
            "responseType": "json",
            "url": "/v1/xerox-notifications/letter",
          },
        ]
      `);
    });
  });
});
