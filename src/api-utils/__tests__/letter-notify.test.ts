// eslint-disable-next-line @typescript-eslint/no-var-requires
const NotifyClient = require("notifications-node-client").NotifyClient;

const sendLetterMock = jest.fn();
jest.mock("notifications-node-client", () => {
  return {
    NotifyClient: jest.fn(),
  };
});
const getSecretValueMock = jest
  .fn()
  .mockResolvedValueOnce("test-key")
  .mockResolvedValueOnce("templateId")
  .mockResolvedValueOnce("test-key")
  .mockResolvedValueOnce("templateId")
  .mockResolvedValueOnce("test-key")
  .mockResolvedValueOnce("templateId");

jest.mock("@nhsbsa/health-charge-exemption-npm-utils-rest", () => ({
  EventType: {
    renewal: "renewal",
  },
  getSecretValue: getSecretValueMock,
}));

import { GovNotifyLetterAddress } from "../models";
import {
  castPersonalisationAddressToGovNotifyLetterAddress,
  triggerLetterNotification,
} from "../letter-notify";
import { EventType } from "@nhsbsa/health-charge-exemption-npm-utils-rest";

const personalisation = {
  address_line_1: "address one",
  address_line_2: "address two",
  address_line_3: "address three",
  certReference: "testReference",
  endDate: "01 January 2024",
  fullName: "Test FullName",
  startDate: "01 January 2023",
};

let personalisationAddress, expectedGovNotifyLetterAddress;
beforeEach(() => {
  personalisationAddress = {
    addressLine1: "firstName lastName",
    addressLine2: "address 1",
    addressLine3: "address 2",
  };
  expectedGovNotifyLetterAddress = {
    address_line_1: "firstName lastName",
    address_line_2: "address 1",
    address_line_3: "address 2",
  };
  process.env.govNotifyApiKey = "test-key";
  NotifyClient.mockImplementation(() => ({ sendLetter: sendLetterMock }));
  sendLetterMock.mockResolvedValue({ id: "letter-sent-id" });
});

describe("letterNotify", () => {
  describe("castPersonalisationAddressToGovNotifyLetterAddress()", () => {
    it.each([3, 4, 5, 6])(
      "should convert PersonalisationAddress To GovNotifyLetterAddress with %p addresses",
      (addressMaxLineNum: number) => {
        // given
        for (let lineNum = 4; lineNum <= addressMaxLineNum; lineNum++) {
          personalisationAddress[`addressLine${lineNum}`] = `address ${
            lineNum - 1
          }`;
          expectedGovNotifyLetterAddress[
            `address_line_${lineNum}`
          ] = `address ${lineNum - 1}`;
        }

        // when
        const actualGovNotifyLetterAddress: GovNotifyLetterAddress =
          castPersonalisationAddressToGovNotifyLetterAddress(
            personalisationAddress,
          );

        // then
        expect(actualGovNotifyLetterAddress).toEqual(
          expectedGovNotifyLetterAddress,
        );
      },
    );
  });

  describe("triggerLetterNotification()", () => {
    it("should call the service with the apiKey and correct template key when event type is renewal", async () => {
      // given
      // when
      await triggerLetterNotification(personalisation, EventType.renewal);

      // then
      expect(getSecretValueMock).toHaveBeenCalledTimes(2);
      expect(getSecretValueMock).toBeCalledWith("GOV_NOTIFY_API_KEY");
      expect(getSecretValueMock).toBeCalledWith(
        "GOV_NOTIFY_LETTER_RENEWAL_TEMPLATE_ID",
      );
      expect(NotifyClient).toHaveBeenCalledTimes(1);
      expect(NotifyClient).toHaveBeenCalledWith("test-key");
    });

    it("should call the govNotify with the correct parameters", async () => {
      // given
      // when
      await triggerLetterNotification(personalisation, EventType.renewal);

      // then
      expect(getSecretValueMock).toHaveBeenCalledTimes(2);
      expect(getSecretValueMock).toBeCalledWith("GOV_NOTIFY_API_KEY");
      expect(getSecretValueMock).toBeCalledWith(
        "GOV_NOTIFY_LETTER_RENEWAL_TEMPLATE_ID",
      );
      expect(sendLetterMock).toHaveBeenCalledWith("templateId", {
        personalisation: {
          address_line_1: "address one",
          address_line_2: "address two",
          address_line_3: "address three",
          certReference: "testReference",
          endDate: "01 January 2024",
          fullName: "Test FullName",
          startDate: "01 January 2023",
        },
        reference: "testReference",
      });
    });

    it("should return the result of the govNotify service", async () => {
      // given
      // when
      const response = await triggerLetterNotification(
        personalisation,
        EventType.renewal,
      );

      // then
      expect(getSecretValueMock).toHaveBeenCalledTimes(2);
      expect(getSecretValueMock).toBeCalledWith("GOV_NOTIFY_API_KEY");
      expect(getSecretValueMock).toBeCalledWith(
        "GOV_NOTIFY_LETTER_RENEWAL_TEMPLATE_ID",
      );
      expect(response).toEqual({ id: "letter-sent-id" });
    });
  });
});
