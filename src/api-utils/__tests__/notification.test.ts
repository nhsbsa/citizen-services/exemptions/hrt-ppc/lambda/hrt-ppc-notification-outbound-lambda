import { NotificationApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { updateNotification } from "../notification";
import { mockXeroxNotificationRecord } from "../../__mocks__/mockData";
import { GovNotifyResponse } from "../models";

let notificationApiSpy: jest.SpyInstance;

beforeEach(() => {
  notificationApiSpy = jest
    .spyOn(NotificationApi.prototype, "makeRequest")
    .mockResolvedValue({});
});

afterEach(() => {
  jest.resetAllMocks();
});

describe("notification", () => {
  describe("updateNotification()", () => {
    const notificationId = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
    const externalNotifyId = "notifyid-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
    const defaultNotificationPatchResponse = {
      status: 204,
      data: {},
    };
    const headers = {
      header: "value",
    };
    describe("govNotify", () => {
      it("should build request data and return an 204 with FAILED when ivalid data", async () => {
        // given
        const responseWithoutStatus: GovNotifyResponse = {
          status: 201,
          data: { id: undefined },
        };
        notificationApiSpy.mockResolvedValue(defaultNotificationPatchResponse); // 204 no content

        // when
        const response = await updateNotification(
          headers,
          notificationId,
          responseWithoutStatus,
        );

        // then
        expect(notificationApiSpy).toHaveBeenCalledTimes(1);
        expect(notificationApiSpy).toHaveBeenCalledWith({
          method: "PATCH",
          url: "/v1/notifications/" + notificationId,
          headers,
          data: { status: "FAILED" },
          responseType: "json",
        });
        expect(notificationApiSpy.mock.calls).toMatchSnapshot();
        expect(response).toStrictEqual(defaultNotificationPatchResponse);
      });

      it.each([200, 201])(
        "should build request data and return a 204 with SENT when success reponse is %s and present externalNotifyId",
        async (status: number) => {
          // given
          const correctNotifyResponse: GovNotifyResponse = {
            status: status,
            otherField: "does nothing",
            data: { id: externalNotifyId },
          };
          notificationApiSpy.mockResolvedValue(
            defaultNotificationPatchResponse,
          ); // 204 no content

          // when
          const response = await updateNotification(
            headers,
            notificationId,
            correctNotifyResponse,
          );

          // then
          expect(notificationApiSpy).toHaveBeenCalledTimes(1);
          expect(notificationApiSpy).toHaveBeenCalledWith({
            method: "PATCH",
            url: "/v1/notifications/" + notificationId,
            headers,
            data: {
              status: "SENT",
              externalNotifyId: externalNotifyId,
            },
            responseType: "json",
          });
          expect(notificationApiSpy.mock.calls).toMatchSnapshot();
          expect(response).toStrictEqual(defaultNotificationPatchResponse);
        },
      );

      it.each([200, 201])(
        "should build request data and return a 204 with FAILED when success reponse is %s but null externalNotifyId",
        async (status: number) => {
          // given
          const correctNotifyResponse: GovNotifyResponse = {
            status: status,
            data: {},
          };
          notificationApiSpy.mockResolvedValue(
            defaultNotificationPatchResponse,
          ); // 204 no content

          // when
          const response = await updateNotification(
            headers,
            notificationId,
            correctNotifyResponse,
          );

          // then
          expect(notificationApiSpy).toHaveBeenCalledTimes(1);
          expect(notificationApiSpy).toHaveBeenCalledWith({
            method: "PATCH",
            url: "/v1/notifications/" + notificationId,
            headers,
            data: { status: "FAILED" },
            responseType: "json",
          });
          expect(notificationApiSpy.mock.calls).toMatchSnapshot();
          expect(response).toStrictEqual(defaultNotificationPatchResponse);
        },
      );
    });

    describe("xerox", () => {
      it("should build request data and return a 204 with SENT when success reponse is a valid xerox response", async () => {
        // given
        notificationApiSpy.mockResolvedValue(defaultNotificationPatchResponse); // 204 no content

        // when
        const response = await updateNotification(
          headers,
          notificationId,
          mockXeroxNotificationRecord,
        );

        // then
        expect(notificationApiSpy).toHaveBeenCalledTimes(1);
        expect(notificationApiSpy).toHaveBeenCalledWith({
          method: "PATCH",
          url: "/v1/notifications/" + notificationId,
          headers,
          data: {
            status: "SENT",
            externalNotifyId: mockXeroxNotificationRecord.id,
          },
          responseType: "json",
        });
        expect(notificationApiSpy.mock.calls).toMatchSnapshot();
        expect(response).toStrictEqual(defaultNotificationPatchResponse);
      });

      it("should build request data and return a 204 with FAILED when success reponse is a invalid xerox response", async () => {
        // given
        notificationApiSpy.mockResolvedValue(defaultNotificationPatchResponse); // 204 no content
        const invalidXeroxResponse = { id: undefined };

        // when
        const response = await updateNotification(
          headers,
          notificationId,
          invalidXeroxResponse,
        );

        // then
        expect(notificationApiSpy).toHaveBeenCalledTimes(1);
        expect(notificationApiSpy).toHaveBeenCalledWith({
          method: "PATCH",
          url: "/v1/notifications/" + notificationId,
          headers,
          data: { status: "FAILED" },
          responseType: "json",
        });
        expect(notificationApiSpy.mock.calls).toMatchSnapshot();
        expect(response).toStrictEqual(defaultNotificationPatchResponse);
      });
    });
  });
});
