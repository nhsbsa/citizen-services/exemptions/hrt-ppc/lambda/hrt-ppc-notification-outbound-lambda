import { EventType } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { parseEventType } from "../model-utils";
import { SQSMessageAttribute } from "aws-lambda";

describe("modelUtils", () => {
  describe("parseEventType()", () => {
    it.each([
      [EventType.issueCertificate, "issue-certificate"],
      [EventType.reissueCertificate, "reissue-certificate"],
      [EventType.renewal, "renewal"],
      [EventType.refund, "refund"],
    ])(
      "should return EventType %p when pass in %p",
      (expectEventType, attributeValue) => {
        // given
        const attribute: SQSMessageAttribute = {
          stringValue: attributeValue,
          dataType: "String",
        };

        // when
        // then
        expect(parseEventType(attribute)).toEqual(expectEventType);
      },
    );

    it.each(["invalid", undefined])(
      "should return undefine when pass in %p",
      (attributeValue) => {
        // given
        const attribute: SQSMessageAttribute = {
          stringValue: attributeValue,
          dataType: "String",
        };

        // when
        // then
        expect(parseEventType(attribute)).toEqual(undefined);
      },
    );
  });
});
