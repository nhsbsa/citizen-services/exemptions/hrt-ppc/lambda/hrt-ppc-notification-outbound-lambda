import {
  getSecretValue,
  PersonalisationAddress,
  XeroxNotificationApi,
  XeroxNotificationResponse,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { HRT_SERVICE, SEND_NOTIFY_EVENT } from "./constants";
import { Channel } from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";

const xeroxNotificationApi = new XeroxNotificationApi();

export type Personalisation = PersonalisationAddress & {
  certificateReference: string;
  certificateStartDate: string;
  certificateEndDate: string;
  firstName: string;
  lastName: string;
};

export async function sendLetterXeroxNotification(
  correlationId: string,
  reference: string,
  personalisation: Personalisation,
): Promise<XeroxNotificationResponse> {
  const headers = {
    "x-api-key": process.env.XEROX_PUBLIC_KEY || "XEROX_PUBLIC_KEY",
    channel: Channel.BATCH,
    "user-id": SEND_NOTIFY_EVENT,
    "correlation-id": correlationId,
    "service-name": HRT_SERVICE,
  };
  const xeroxNotificationTemplateId = await getSecretValue(
    process.env.XEROX_NOTIFICATION_TEMPLATE_ID_SM_ID ||
      "XEROX_NOTIFICATION_TEMPLATE_ID",
  );
  return await xeroxNotificationApi.makeRequest({
    method: "POST",
    url: "/v1/xerox-notifications/letter",
    data: {
      templateId: xeroxNotificationTemplateId,
      reference,
      personalisation,
    },
    headers,
    responseType: "json",
  });
}
