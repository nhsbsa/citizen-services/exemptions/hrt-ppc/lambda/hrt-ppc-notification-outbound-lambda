import { CertificateApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { CertificateResponse } from "./models";

const certificateApi = new CertificateApi();

export async function getCertificateById(
  certificateId,
  headers,
): Promise<CertificateResponse> {
  return await certificateApi.makeRequest({
    method: "GET",
    url: "/v1/certificates/" + certificateId,
    headers,
    responseType: "json",
  });
}
