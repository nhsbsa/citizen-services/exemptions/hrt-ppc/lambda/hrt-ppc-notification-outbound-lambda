import { NotificationApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import {
  GovNotifyResponse,
  NotificationPatchData,
  XeroxResponse,
} from "./models";

const notificationApi = new NotificationApi();

async function updateNotificationById(
  notificationId,
  headers,
  data,
): Promise<void> {
  return notificationApi.makeRequest({
    method: "PATCH",
    url: `/v1/notifications/${notificationId}`,
    headers,
    data: data,
    responseType: "json",
  });
}

/**
 * Function to build and send the correct data to the Notification API PATCH endpoint.
 *
 * @param headers request headers
 * @param notificationId the notification id
 * @param response the response object from {@link GovNotifyResponse} or {@link XeroxResponse}
 * @returns void
 */
export async function updateNotification(
  headers,
  notificationId: string,
  response: GovNotifyResponse | XeroxResponse,
): Promise<void> {
  const externalNotifyId: string | undefined =
    "data" in response
      ? (response.data as GovNotifyResponse["data"]).id
      : response.id;
  const data: NotificationPatchData = {
    status: "FAILED",
  };

  if (externalNotifyId != null) {
    (data.status = "SENT"), (data.externalNotifyId = externalNotifyId);
  }

  return await updateNotificationById(notificationId, headers, data);
}
