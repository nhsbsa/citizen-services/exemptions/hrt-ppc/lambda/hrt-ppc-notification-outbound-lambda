// eslint-disable-next-line @typescript-eslint/no-var-requires
const NotifyClient = require("notifications-node-client").NotifyClient;
import { loggerWithContext } from "@nhsbsa/hrt-ppc-npm-logging";
import { GovNotifyLetterAddress } from "./models";
import {
  EventType,
  getSecretValue,
  PersonalisationAddress,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

export function castPersonalisationAddressToGovNotifyLetterAddress(
  personalisationAddress: PersonalisationAddress,
): GovNotifyLetterAddress {
  const mandatoryAddressFields: GovNotifyLetterAddress = {
    address_line_1: personalisationAddress.addressLine1,
    address_line_2: personalisationAddress.addressLine2,
    address_line_3: personalisationAddress.addressLine3,
  };
  /**
   * Copy addressLine 4 to 7 in PersonalisationAddress to address_line_4 to 6.
   * And ignore lines from first not existed addressLine, in other word,
   * if the addressLineX does not exist then from addressLineX to 6 will be ignored
   * e.g. if the addressLine4 does not exist then addressLine4 to 6 will be ignored.
   * if the addressLine5 does not exist then addressLine5 to 6 will be ignored.
   */
  for (let nthAddress = 4; nthAddress < 7; nthAddress++) {
    if (personalisationAddress[`addressLine${nthAddress}`]) {
      mandatoryAddressFields[`address_line_${nthAddress}`] =
        personalisationAddress[`addressLine${nthAddress}`];
    } else {
      break;
    }
  }
  return mandatoryAddressFields;
}

export async function triggerLetterNotification(
  personalisation,
  eventType: EventType | undefined,
) {
  const govApiKey = await getSecretValue(
    process.env.GOV_NOTIFY_API_KEY_SM_ID || "GOV_NOTIFY_API_KEY",
  );

  let govNotificationTemplateId;
  if (eventType === EventType.renewal) {
    govNotificationTemplateId = await getSecretValue(
      process.env.GOV_NOTIFY_LETTER_RENEWAL_TEMPLATE_ID_SM_ID ||
        "GOV_NOTIFY_LETTER_RENEWAL_TEMPLATE_ID",
    );
  }

  const notifyClient = new NotifyClient(govApiKey);
  const { certReference } = personalisation;
  loggerWithContext().info(
    `Received [Certificate reference: ${certReference}}]`,
  );
  return notifyClient.sendLetter(govNotificationTemplateId, {
    personalisation: personalisation,
    reference: certReference,
  });
}
