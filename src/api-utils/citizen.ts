import {
  CitizenApi,
  CitizenResponse,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

const citizenApi = new CitizenApi();

export async function getCitizenById(
  citizenId,
  headers,
): Promise<CitizenResponse> {
  return citizenApi.makeRequest({
    method: "GET",
    url: "/v1/citizens/" + citizenId,
    headers,
    responseType: "json",
  });
}
