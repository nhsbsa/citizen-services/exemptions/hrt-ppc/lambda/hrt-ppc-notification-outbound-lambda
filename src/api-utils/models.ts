import { Meta } from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";
import {
  CertificateStatus,
  CertificateType,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

export interface CertificateResponse {
  id: string;
  citizenId: string;
  reference: string;
  type: CertificateType;
  applicationDate: string;
  duration: string;
  startDate: string;
  endDate: string;
  cost: number;
  status: CertificateStatus;
  pharmacyId?: string;
  _meta: Meta;
  _links: unknown;
}

/**
 * Type for the data body sent to the Notification PATCH endpoint.
 */
export interface NotificationPatchData {
  status: string;
  externalNotifyId?: string;
}

/**
 * Response type returned when triggerEmailNotification is called.
 * We only care about the data object here. Other keys can be accepted if returned.
 */
export interface GovNotifyResponse {
  status?: number;
  data: {
    id?: string;
    [key: string]: unknown;
  };
  [key: string]: unknown;
}

/**
 * Response type returned when sendLetterXeroxNotification is called.
 * We only care about the id value here. Other keys can be accepted if returned.
 */
export interface XeroxResponse {
  id?: string;
  [key: string]: unknown;
}

/**
 * this interface is used to define gov notify address. It must include at least 3 lines.
 */
export interface GovNotifyLetterAddress {
  address_line_1: string;
  address_line_2: string;
  address_line_3: string;
  address_line_4?: string;
  address_line_5?: string;
  address_line_6?: string;
  address_line_7?: string;
}
