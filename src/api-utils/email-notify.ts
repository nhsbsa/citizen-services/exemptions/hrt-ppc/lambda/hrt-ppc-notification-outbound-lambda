// eslint-disable-next-line @typescript-eslint/no-var-requires
const NotifyClient = require("notifications-node-client").NotifyClient;
import { loggerWithContext } from "@nhsbsa/hrt-ppc-npm-logging";
import {
  EventType,
  getSecretValue,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

export async function triggerEmailNotification(
  personalisation,
  emailAddress: string,
  eventType: EventType | undefined,
) {
  const govApiKey = await getSecretValue(
    process.env.GOV_NOTIFY_API_KEY_SM_ID || "GOV_NOTIFY_API_KEY",
  );

  let govNotificationTemplateId;
  if (eventType === EventType.issueCertificate) {
    govNotificationTemplateId = await getSecretValue(
      process.env.GOV_NOTIFY_EMAIL_ISSUE_TEMPLATE_ID_SM_ID ||
        "GOV_NOTIFY_EMAIL_ISSUE_TEMPLATE_ID",
    );
  }
  if (eventType === EventType.reissueCertificate) {
    govNotificationTemplateId = await getSecretValue(
      process.env.GOV_NOTIFY_EMAIL_REISSUE_TEMPLATE_ID_SM_ID ||
        "GOV_NOTIFY_EMAIL_REISSUE_TEMPLATE_ID",
    );
  }
  if (eventType === EventType.renewal) {
    govNotificationTemplateId = await getSecretValue(
      process.env.GOV_NOTIFY_EMAIL_RENEWAL_TEMPLATE_ID_SM_ID ||
        "GOV_NOTIFY_EMAIL_RENEWAL_TEMPLATE_ID",
    );
  }

  const notifyClient = new NotifyClient(govApiKey);
  const { certReference } = personalisation;
  loggerWithContext().info(
    `Received [Certificate reference: ${certReference}}]`,
  );
  return notifyClient.sendEmail(govNotificationTemplateId, emailAddress, {
    personalisation: personalisation,
    reference: certReference,
  });
}
