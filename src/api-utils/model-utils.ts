import { SQSMessageAttribute } from "aws-lambda";
import { EventType } from "@nhsbsa/health-charge-exemption-npm-utils-rest";

export function parseEventType(
  attribute: SQSMessageAttribute,
): EventType | undefined {
  const eventTypeValue = attribute?.stringValue;
  if (
    Object.values(EventType).some((value: string) => value === eventTypeValue)
  ) {
    return <EventType>eventTypeValue;
  }
  return undefined;
}
