import { CertificateResponse } from "../api-utils";
import { Channel } from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";
import {
  CertificateStatus,
  CertificateType,
  CitizenResponse,
  XeroxNotificationResponse,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

export const mockNoNotificationId = {
  certificate: {
    id: "c0a80005-859c-1146-8185-b09850de0018",
  },
};

export const mockNoCertificateIdValue = {
  certificate: {
    id: "",
  },
  notification: {
    id: "c0a80005-859c-1146-8185-b09850de0021",
  },
};

export const mockNoNotificationIdValue = {
  certificate: {
    id: "c0a80005-859c-1146-8185-b09850de0018",
  },
  notification: {
    id: "",
  },
};

export const mockNoCertificateId = {
  notification: {
    id: "c0a80005-859c-1146-8185-b09850de0021",
  },
};

export const mockEmailResponse = {
  data: {
    id: "mock-email-sent-id",
  },
};

export const mockLetterResponse = {
  data: {
    id: "mock-letter-sent-id",
  },
};

export const mockSQSRecord = {
  messageId: "059f36b4-87a3-44ab-83d2-661975830a7d",
  receiptHandle: "AQEBwJnKyrHigUMZj6rYigCgxlaS3SLy0a",
  body: '{"notification":{"id":"c0a80005-859c-1146-8185-b09850de0021"},"certificate":{"id":"c0a80005-859c-1146-8185-b09850de0018"}}',
  attributes: {
    ApproximateReceiveCount: "1",
    SentTimestamp: "1545082649183",
    SenderId: "AIDAIENQZJOLO23YVJ4VO",
    ApproximateFirstReceiveTimestamp: "1545082649185",
  },
  messageAttributes: {
    eventName: {
      dataType: "string",
      stringValue: "notificationCreation",
    },
    source: {
      dataType: "string",
      stringValue: "health-charge-exemption-notification-api",
    },
    certificateType: {
      dataType: "string",
      stringValue: "HRT_PPC",
    },
    eventType: {
      dataType: "string",
      stringValue: "issue-certificate",
    },
    deliveryType: {
      dataType: "string",
      stringValue: "email",
    },
    correlationId: {
      dataType: "string",
      stringValue: "a580305a-dae9-46a4-a420-ddee91054d7a",
    },
  },
  md5OfBody: "098f6bcd4621d373cade4e832627b4f6",
  eventSource: "aws:sqs",
  eventSourceARN: "arn:aws:sqs:us-east-2:123456789012:my-queue",
  awsRegion: "us-east-2",
};

export const mockCertificateRecord: CertificateResponse = {
  id: "c0a80005-859c-1146-8185-b09850de0018",
  citizenId: "c0a80005-8577-17a8-8185-7717e2540000",
  reference: "HRTF1FC752F",
  type: CertificateType.HRT_PPC,
  applicationDate: "2022-12-12",
  duration: "P12M",
  startDate: "2022-12-12",
  endDate: "2023-12-12",
  cost: 1870,
  status: CertificateStatus.PENDING,
  _meta: {
    channel: Channel.ONLINE,
    createdTimestamp: new Date("2023-06-01T14:31:26.423507"),
    createdBy: "ONLINE",
    updatedTimestamp: new Date("2023-06-01T14:31:26.423507"),
    updatedBy: "ONLINE",
  },
  _links: {
    self: {
      href: "http://localhost:8090/v1/certificates/ac14c125-8877-17b0-8188-7727f1570001",
    },
    certificate: {
      href: "http://localhost:8090/v1/certificates/ac14c125-8877-17b0-8188-7727f1570001",
    },
  },
};

export const mockCitizenRecord: CitizenResponse = {
  id: "c0a80005-859c-1146-8185-b09850de0018",
  firstName: "HRT",
  lastName: "EXPIRED",
  dateOfBirth: new Date("1997-03-24"),
  addresses: [
    {
      id: "c0a80005-859c-1146-8185-b09850e40019",
      type: "POSTAL",
      addressLine1: "Stella House",
      addressLine2: "Goldcrest Way",
      townOrCity: "Newcastle upon Tyne",
      country: "England",
      postcode: "NE15 8NY",
      _meta: {
        channel: Channel.ONLINE,
        createdTimestamp: new Date("2023-01-14T14:04:10.340737"),
        createdBy: "POSTMAN",
        updatedTimestamp: new Date("2023-01-14T14:04:10.340737"),
        updatedBy: "POSTMAN",
      },
    },
  ],
  emails: [
    {
      id: "c0a80005-859c-1146-8185-b09850e6001a",
      type: "PERSONAL",
      emailAddress: "test@test.com",
      _meta: {
        channel: Channel.ONLINE,
        createdTimestamp: new Date("2023-01-14T13:53:23.930323"),
        createdBy: "POSTMAN",
        updatedTimestamp: new Date("2023-01-14T13:53:23.930323"),
        updatedBy: "POSTMAN",
      },
    },
  ],
  _meta: {
    channel: Channel.ONLINE,
    createdTimestamp: new Date("2023-01-14T13:53:23.879515"),
    createdBy: "POSTMAN",
    updatedTimestamp: new Date("2023-01-14T13:53:23.879515"),
    updatedBy: "POSTMAN",
  },
};

export const mockCitizenRecordNoEmail: CitizenResponse = {
  id: "c0a80005-859c-1146-8185-b09850de0018",
  firstName: "HRT",
  lastName: "EXPIRED",
  dateOfBirth: new Date("1997-03-24"),
  addresses: [
    {
      id: "c0a80005-859c-1146-8185-b09850e40019",
      type: "POSTAL",
      addressLine1: "Stella House",
      addressLine2: "Goldcrest Way",
      townOrCity: "Newcastle upon Tyne",
      country: "England",
      postcode: "NE15 8NY",
      _meta: {
        channel: Channel.ONLINE,
        createdTimestamp: new Date("2023-01-14T14:04:10.340737"),
        createdBy: "POSTMAN",
        updatedTimestamp: new Date("2023-01-14T14:04:10.340737"),
        updatedBy: "POSTMAN",
      },
    },
  ],
  emails: [],
  _meta: {
    channel: Channel.ONLINE,
    createdTimestamp: new Date("2023-01-14T13:53:23.879515"),
    createdBy: "POSTMAN",
    updatedTimestamp: new Date("2023-01-14T13:53:23.879515"),
    updatedBy: "POSTMAN",
  },
};

export const mockXeroxNotificationRecord: XeroxNotificationResponse = {
  id: "e1a328ae-efa8-482f-bb40-5963f7aab894",
  transactionDate: new Date("2022-09-14"),
  templateId: "55271aac-a2b4-4635-8543-234f35b4aaf2",
  personalisation: {
    addressLine1: "Address line one",
    addressLine2: "Address line two",
    addressLine3: "Address line three",
    addressLine4: "Address line four",
    addressLine5: "Address line five",
    addressLine6: "Address line six",
    addressLine7: "NE15 8NY",
  },
  reference: "HRTABCD123",
  correlationId: "e2a328ae-efa8-482f-bb40-5963f7aab885",
  status: "PENDING",
  statusUpdatedAt: new Date("2023-06-02T14:44:27.646Z"),
  _meta: {
    createdTimestamp: new Date("2023-06-02T14:44:27.646Z"),
    createdBy: "ONLINE",
    updatedTimestamp: new Date("2023-06-02T14:44:27.646Z"),
    updatedBy: "ONLINE",
  },
  _links: {
    self: {
      href: "/v1/xerox-notifications/letter/e1a328ae-efa8-482f-bb40-5963f7aab894",
    },
    letter: {
      href: "/v1/xerox-notifications/letter/e1a328ae-efa8-482f-bb40-5963f7aab894",
    },
    template: {
      href: "/v1/xerox-notifications/templates/55271aac-a2b4-4635-8543-234f35b4aaf2",
    },
  },
};
