import { SQSEvent } from "aws-lambda";
import { handler } from "../index";
import { processRecord } from "../process-sqs-event-record";
import mockEventSingleRecord from "../../events/event-single-record-success.json";
import mockEventMultipleRecords from "../../events/event-multiple-records.json";

jest.mock("../process-sqs-event-record");

let event: SQSEvent;
const mockProcessRecord = processRecord as jest.MockedFunction<
  typeof processRecord
>;

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date("2020-01-01"));
  event = JSON.parse(JSON.stringify(mockEventSingleRecord));
  mockProcessRecord.mockClear();
  mockProcessRecord.mockResolvedValue();
});

describe("handler", () => {
  it("should handle the event successfully without any errors", async () => {
    await expect(handler(event)).resolves.not.toThrowError();
    expect(mockProcessRecord).toBeCalledTimes(1);
  });

  it("should have error when more than one event", async () => {
    event = JSON.parse(JSON.stringify(mockEventMultipleRecords));
    await expect(handler(event)).rejects.toThrowError(
      "Expected only 1 record but found 3",
    );
    expect(mockProcessRecord).not.toBeCalled();
  });
});
