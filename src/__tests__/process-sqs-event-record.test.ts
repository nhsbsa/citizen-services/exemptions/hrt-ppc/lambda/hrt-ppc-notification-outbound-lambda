import { processRecord } from "../process-sqs-event-record";
import {
  mockCertificateRecord,
  mockCitizenRecord,
  mockNoCertificateId,
  mockNoCertificateIdValue,
  mockNoNotificationId,
  mockNoNotificationIdValue,
  mockSQSRecord,
  mockEmailResponse,
  mockCitizenRecordNoEmail,
  mockXeroxNotificationRecord,
  mockLetterResponse,
} from "../__mocks__/mockData";
import { randomUUID } from "crypto";
import * as certificate from "../api-utils/certificate";
import * as citizen from "../api-utils/citizen";
import * as emailNotify from "../api-utils/email-notify";
import * as letterNotify from "../api-utils/letter-notify";
import * as xeroxNotification from "../api-utils/xerox-notification";
import * as notification from "../api-utils/notification";
import {
  CertificateType,
  DeliveryType,
  EventType,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

jest.mock("crypto");

let getCertificateByIdSpy: jest.SpyInstance;
let getCitizenByIdSpy: jest.SpyInstance;
let triggerEmailNotificationSpy: jest.SpyInstance;
let triggerLetterNotificationSpy: jest.SpyInstance;
let sendLetterXeroxNotificationSpy: jest.SpyInstance;
let updateNotificationSpy: jest.SpyInstance;

const mockRandomUUID = randomUUID as jest.MockedFunction<typeof randomUUID>;

const expectedCertificateCall = `
  [
    "c0a80005-859c-1146-8185-b09850de0018",
    {
      "channel": "BATCH",
      "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
      "user-id": "SEND_NOTIFY_EVENT",
    },
  ]
`;

const expectedCitizenCall = `
  [
    "c0a80005-8577-17a8-8185-7717e2540000",
    {
      "channel": "BATCH",
      "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
      "user-id": "SEND_NOTIFY_EVENT",
    },
  ]
`;

const expectedXeroxUpdateNotification = `
[
  {
    "channel": "BATCH",
    "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
    "user-id": "SEND_NOTIFY_EVENT",
  },
  "c0a80005-859c-1146-8185-b09850de0021",
  {
    "_links": {
      "letter": {
        "href": "/v1/xerox-notifications/letter/e1a328ae-efa8-482f-bb40-5963f7aab894",
      },
      "self": {
        "href": "/v1/xerox-notifications/letter/e1a328ae-efa8-482f-bb40-5963f7aab894",
      },
      "template": {
        "href": "/v1/xerox-notifications/templates/55271aac-a2b4-4635-8543-234f35b4aaf2",
      },
    },
    "_meta": {
      "createdBy": "ONLINE",
      "createdTimestamp": 2023-06-02T14:44:27.646Z,
      "updatedBy": "ONLINE",
      "updatedTimestamp": 2023-06-02T14:44:27.646Z,
    },
    "correlationId": "e2a328ae-efa8-482f-bb40-5963f7aab885",
    "id": "e1a328ae-efa8-482f-bb40-5963f7aab894",
    "personalisation": {
      "addressLine1": "Address line one",
      "addressLine2": "Address line two",
      "addressLine3": "Address line three",
      "addressLine4": "Address line four",
      "addressLine5": "Address line five",
      "addressLine6": "Address line six",
      "addressLine7": "NE15 8NY",
    },
    "reference": "HRTABCD123",
    "status": "PENDING",
    "statusUpdatedAt": 2023-06-02T14:44:27.646Z,
    "templateId": "55271aac-a2b4-4635-8543-234f35b4aaf2",
    "transactionDate": 2022-09-14T00:00:00.000Z,
  },
]
`;

let sqsRecord;
beforeEach(() => {
  sqsRecord = JSON.parse(JSON.stringify(mockSQSRecord));

  getCertificateByIdSpy = jest
    .spyOn(certificate, "getCertificateById")
    .mockResolvedValue(mockCertificateRecord);

  getCitizenByIdSpy = jest
    .spyOn(citizen, "getCitizenById")
    .mockResolvedValue(mockCitizenRecord);

  triggerEmailNotificationSpy = jest
    .spyOn(emailNotify, "triggerEmailNotification")
    .mockResolvedValue(mockEmailResponse);

  triggerLetterNotificationSpy = jest
    .spyOn(letterNotify, "triggerLetterNotification")
    .mockResolvedValue(mockLetterResponse);

  sendLetterXeroxNotificationSpy = jest
    .spyOn(xeroxNotification, "sendLetterXeroxNotification")
    .mockResolvedValue(mockXeroxNotificationRecord);

  mockRandomUUID.mockReturnValue("a580305a-dae9-46a4-a420-ddee91054d7a");

  updateNotificationSpy = jest
    .spyOn(notification, "updateNotification")
    .mockResolvedValue(); // returns Promise<void>
});

afterEach(() => {
  jest.resetAllMocks();
  jest.restoreAllMocks();
  jest.resetModules();
});

describe("processRecord()", () => {
  describe("should process the sqs record and get certificate and citizen information", () => {
    it.each([
      [EventType.issueCertificate, DeliveryType.email],
      [EventType.reissueCertificate, DeliveryType.email],
      [EventType.renewal, DeliveryType.email],
    ])(
      "should process the sqs record and get certificate for event type %p and delivery type %p",
      async (eventType, deliveryType) => {
        // given
        sqsRecord.messageAttributes.eventType.stringValue =
          eventType.toString();
        sqsRecord.messageAttributes.deliveryType.stringValue =
          deliveryType.toString();

        // when
        // then
        await expect(processRecord(sqsRecord)).resolves.not.toThrowError();
        expect(getCertificateByIdSpy).toBeCalledTimes(1);
        expect(updateNotificationSpy).toBeCalledTimes(1);
        expect(updateNotificationSpy.mock.calls[0]).toMatchInlineSnapshot(`
        [
          {
            "channel": "BATCH",
            "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
            "user-id": "SEND_NOTIFY_EVENT",
          },
          "c0a80005-859c-1146-8185-b09850de0021",
          {
            "data": {
              "id": "mock-email-sent-id",
            },
          },
        ]
        `);
        expect(triggerEmailNotificationSpy).toBeCalledTimes(1);
        expect(getCertificateByIdSpy.mock.calls[0]).toMatchInlineSnapshot(
          expectedCertificateCall,
        );
        expect(getCitizenByIdSpy).toBeCalledTimes(1);
        expect(getCitizenByIdSpy.mock.calls[0]).toMatchInlineSnapshot(
          expectedCitizenCall,
        );
        expect(triggerEmailNotificationSpy.mock.calls[0])
          .toMatchInlineSnapshot(`
          [
            {
              "certReference": "HRTF1FC752F",
              "endDate": "12 December 2023",
              "fullName": "HRT EXPIRED",
              "startDate": "12 December 2022",
            },
            "test@test.com",
            "${eventType}",
          ]
      `);
        expect(sendLetterXeroxNotificationSpy).not.toBeCalled();
      },
    );

    it.each([
      [EventType.issueCertificate, DeliveryType.letter],
      [EventType.reissueCertificate, DeliveryType.letter],
    ])(
      "should process the sqs record and get certificate for event type %p  and delivery type %p",
      async (eventType, deliveryType) => {
        // given
        sqsRecord.messageAttributes.eventType.stringValue =
          eventType.toString();
        sqsRecord.messageAttributes.deliveryType.stringValue =
          deliveryType.toString();

        // when
        // then
        await expect(processRecord(sqsRecord)).resolves.not.toThrowError();
        expect(getCertificateByIdSpy).toBeCalledTimes(1);
        expect(triggerEmailNotificationSpy).toBeCalledTimes(0);
        expect(sendLetterXeroxNotificationSpy).toBeCalledTimes(1);
        expect(updateNotificationSpy).toBeCalledTimes(1);
        expect(updateNotificationSpy.mock.calls[0]).toMatchInlineSnapshot(
          expectedXeroxUpdateNotification,
        );
        expect(getCertificateByIdSpy.mock.calls[0]).toMatchInlineSnapshot(
          expectedCertificateCall,
        );
        expect(getCitizenByIdSpy).toBeCalledTimes(1);
        expect(getCitizenByIdSpy.mock.calls[0]).toMatchInlineSnapshot(
          expectedCitizenCall,
        );
        expect(
          sendLetterXeroxNotificationSpy.mock.calls[0][0],
        ).toMatchInlineSnapshot(`"a580305a-dae9-46a4-a420-ddee91054d7a"`);
        expect(
          sendLetterXeroxNotificationSpy.mock.calls[0][1],
        ).toMatchInlineSnapshot(`"HRTF1FC752F"`);
        expect(sendLetterXeroxNotificationSpy.mock.calls[0][2]).toMatchObject({
          addressLine1: "HRT EXPIRED",
          addressLine2: "Stella House",
          addressLine3: "Goldcrest Way",
          addressLine4: "Newcastle upon Tyne",
          addressLine5: "England",
          addressLine6: "NE15 8NY",
          certificateEndDate: "12122023",
          certificateReference: "HRTF1FC752F",
          certificateStartDate: "12122022",
          firstName: "HRT",
          lastName: "EXPIRED",
        });
      },
    );

    it("should process the sqs record and get certificate for event type renewal and delivery type letter", async () => {
      // given
      sqsRecord.messageAttributes.eventType.stringValue =
        EventType.renewal.toString();
      sqsRecord.messageAttributes.deliveryType.stringValue =
        DeliveryType.letter.toString();

      // when
      // then
      await expect(processRecord(sqsRecord)).resolves.not.toThrowError();
      expect(getCertificateByIdSpy).toBeCalledTimes(1);
      expect(getCertificateByIdSpy.mock.calls[0]).toMatchInlineSnapshot(
        expectedCertificateCall,
      );
      expect(updateNotificationSpy).toBeCalledTimes(1);
      expect(updateNotificationSpy.mock.calls[0]).toMatchInlineSnapshot(`
        [
          {
            "channel": "BATCH",
            "correlation-id": "a580305a-dae9-46a4-a420-ddee91054d7a",
            "user-id": "SEND_NOTIFY_EVENT",
          },
          "c0a80005-859c-1146-8185-b09850de0021",
          {
            "data": {
              "id": "mock-letter-sent-id",
            },
          },
        ]
        `);
      expect(getCitizenByIdSpy).toBeCalledTimes(1);
      expect(getCitizenByIdSpy.mock.calls[0]).toMatchInlineSnapshot(
        expectedCitizenCall,
      );
      expect(triggerLetterNotificationSpy).toBeCalledTimes(1);
      expect(triggerLetterNotificationSpy.mock.calls[0]).toMatchInlineSnapshot(`
        [
          {
            "address_line_1": "HRT EXPIRED",
            "address_line_2": "Stella House",
            "address_line_3": "Goldcrest Way",
            "address_line_4": "Newcastle upon Tyne",
            "address_line_5": "England",
            "address_line_6": "NE15 8NY",
            "certReference": "HRTF1FC752F",
            "endDate": "12 December 2023",
            "fullName": "HRT EXPIRED",
            "startDate": "12 December 2022",
          },
          "${EventType.renewal}",
        ]
      `);
      expect(sendLetterXeroxNotificationSpy).not.toBeCalled();
    });

    it.each([
      [EventType.issueCertificate, DeliveryType.letter],
      [EventType.reissueCertificate, DeliveryType.letter],
    ])(
      "should process sqs record and not updateNotification when 409 present for event type %p and delivery type %p",
      async (eventType, deliveryType) => {
        // given
        sendLetterXeroxNotificationSpy = jest
          .spyOn(xeroxNotification, "sendLetterXeroxNotification")
          .mockRejectedValueOnce({
            statusCode: 409,
          });
        sqsRecord.messageAttributes.eventType.stringValue =
          eventType.toString();
        sqsRecord.messageAttributes.deliveryType.stringValue =
          deliveryType.toString();

        // when
        await expect(processRecord(sqsRecord)).rejects.toEqual({
          statusCode: 409,
        });

        // then
        expect(getCertificateByIdSpy).toBeCalledTimes(1);
        expect(triggerEmailNotificationSpy).toBeCalledTimes(0);
        expect(sendLetterXeroxNotificationSpy).toBeCalledTimes(1);
        expect(updateNotificationSpy).toBeCalledTimes(0);
        expect(updateNotificationSpy).not.toHaveBeenCalled();
        expect(getCertificateByIdSpy.mock.calls[0]).toMatchInlineSnapshot(
          expectedCertificateCall,
        );
        expect(getCitizenByIdSpy).toBeCalledTimes(1);
        expect(getCitizenByIdSpy.mock.calls[0]).toMatchInlineSnapshot(
          expectedCitizenCall,
        );
        expect(
          sendLetterXeroxNotificationSpy.mock.calls[0][0],
        ).toMatchInlineSnapshot(`"a580305a-dae9-46a4-a420-ddee91054d7a"`);
        expect(
          sendLetterXeroxNotificationSpy.mock.calls[0][1],
        ).toMatchInlineSnapshot(`"HRTF1FC752F"`);
        expect(sendLetterXeroxNotificationSpy.mock.calls[0][2]).toMatchObject({
          addressLine1: "HRT EXPIRED",
          addressLine2: "Stella House",
          addressLine3: "Goldcrest Way",
          addressLine4: "Newcastle upon Tyne",
          addressLine5: "England",
          addressLine6: "NE15 8NY",
          certificateEndDate: "12122023",
          certificateReference: "HRTF1FC752F",
          certificateStartDate: "12122022",
          firstName: "HRT",
          lastName: "EXPIRED",
        });
      },
    );
  });

  describe("should not process the sqs record for invalid event type", () => {
    it.each(["refund", "invalid"])(
      "should not process the sqs record for event type %p",
      async (eventType) => {
        // given
        sqsRecord.messageAttributes.eventType.stringValue = eventType;

        // when
        // then
        await expect(processRecord(sqsRecord)).rejects.toThrowError(
          new Error(
            "Invalid SQS message attributes, event type can only be " +
              EventType.issueCertificate.toString() +
              " or " +
              EventType.reissueCertificate.toString() +
              " or " +
              EventType.renewal.toString(),
          ),
        );
        expect(getCertificateByIdSpy).not.toBeCalled();
        expect(getCitizenByIdSpy).not.toBeCalled();
      },
    );
  });

  describe("should not process the sqs record for invalid certificate type", () => {
    it.each(["PPC"])(
      "should not process the sqs record for certificate type %p",
      async (certificateType) => {
        // given
        sqsRecord.messageAttributes.certificateType.stringValue =
          certificateType;

        // when
        // then
        await expect(processRecord(sqsRecord)).rejects.toThrowError(
          new Error(
            "Invalid SQS message attributes, certificate type can only be " +
              CertificateType.HRT_PPC.toString(),
          ),
        );
        expect(getCertificateByIdSpy).not.toBeCalled();
        expect(getCitizenByIdSpy).not.toBeCalled();
      },
    );
  });

  describe("should not process the sqs record for invalid delivery type", () => {
    it.each(["Email", "fax"])(
      "should not process the sqs record for delivery type %p",
      async (deliveryType) => {
        // given
        sqsRecord.messageAttributes.deliveryType.stringValue = deliveryType;

        //when
        //then
        await expect(processRecord(sqsRecord)).rejects.toThrowError(
          new Error(
            "Invalid SQS message attributes, delivery type can only be " +
              DeliveryType.email.toString() +
              " or " +
              DeliveryType.letter.toString(),
          ),
        );
        expect(getCertificateByIdSpy).not.toBeCalled();
        expect(getCitizenByIdSpy).not.toBeCalled();
      },
    );
  });

  it("should throw an error if certificate api throws an error", async () => {
    // given
    getCertificateByIdSpy = jest
      .spyOn(certificate, "getCertificateById")
      .mockRejectedValue(new Error("get certificate error occurred"));

    // when
    // then
    await expect(processRecord(sqsRecord)).rejects.toThrowError(
      new Error("get certificate error occurred"),
    );
  });

  it("should throw an error if citizen api throws an error", async () => {
    // given
    getCitizenByIdSpy = jest
      .spyOn(citizen, "getCitizenById")
      .mockRejectedValue(new Error("get citizen error occurred"));

    // when
    // then
    await expect(processRecord(sqsRecord)).rejects.toThrowError(
      new Error("get citizen error occurred"),
    );
  });

  describe("should not process the sqs record when unable to parse body", () => {
    it.each(["", "test body"])(
      "should throw an error if message body is %p",
      async (bodyMessage) => {
        // given
        sqsRecord.body = bodyMessage;

        // when
        // then
        await expect(processRecord(sqsRecord)).rejects.toThrowError(
          new Error(
            "Unable to parse record body: " + bodyMessage + " with error {}",
          ),
        );
        expect(getCertificateByIdSpy).not.toBeCalled();
        expect(getCitizenByIdSpy).not.toBeCalled();
      },
    );
  });

  describe("should not process the sqs record when missing data", () => {
    it.each([
      mockNoNotificationId,
      mockNoNotificationIdValue,
      mockNoCertificateIdValue,
      mockNoCertificateId,
    ])(
      "should throw an error if record body is missing any data",
      async (bodyMessage) => {
        // given
        sqsRecord.body = JSON.stringify(bodyMessage);

        // when
        // then
        await expect(processRecord(sqsRecord)).rejects.toThrowError(
          new Error(
            "Invalid record body, must contain notificationId, certificateId",
          ),
        );
        expect(getCertificateByIdSpy).not.toBeCalled();
        expect(getCitizenByIdSpy).not.toBeCalled();
      },
    );
  });

  it("should throw an error if correlation id is missing", async () => {
    // given
    sqsRecord.messageAttributes = {
      eventName: {
        dataType: "string",
        stringValue: "notificationCreation",
      },
      source: {
        dataType: "string",
        stringValue: "health-charge-exemption-notification-api",
      },
      certificateType: {
        dataType: "string",
        stringValue: "HRT_PPC",
      },
      eventType: {
        dataType: "string",
        stringValue: "issue-certificate",
      },
      deliveryType: {
        dataType: "string",
        stringValue: "email",
      },
    };

    // when
    // then
    await expect(processRecord(sqsRecord)).rejects.toThrow(
      new Error("Invalid SQS message attributes, must contain correlationId"),
    );
    expect(getCertificateByIdSpy).not.toBeCalled();
    expect(getCitizenByIdSpy).not.toBeCalled();
  });

  it("should throw an error if email is missing and delivery type is email", async () => {
    // given
    sqsRecord.messageAttributes.deliveryType.stringValue = "email";
    getCitizenByIdSpy = jest
      .spyOn(citizen, "getCitizenById")
      .mockResolvedValue(mockCitizenRecordNoEmail);

    // when
    // then
    await expect(processRecord(sqsRecord)).rejects.toThrowError(
      new Error("Preference was email but citizen email missing"),
    );
  });

  it("should throw an error if gov.uk notify throws an error for email", async () => {
    // given
    sqsRecord.messageAttributes.deliveryType.stringValue = "email";
    triggerEmailNotificationSpy = jest
      .spyOn(emailNotify, "triggerEmailNotification")
      .mockRejectedValue(new Error("Notify error occurred"));

    // when
    // then
    await expect(processRecord(sqsRecord)).rejects.toThrowError(
      new Error("Notify error occurred"),
    );
  });

  it("should throw an error if gov.uk notify throws an error for delivery type letter and event type renewal", async () => {
    // given
    sqsRecord.messageAttributes.deliveryType.stringValue = "letter";
    sqsRecord.messageAttributes.eventType.stringValue = "renewal";
    triggerLetterNotificationSpy = jest
      .spyOn(letterNotify, "triggerLetterNotification")
      .mockRejectedValue(new Error("Notify error occurred"));

    // when
    // then
    await expect(processRecord(sqsRecord)).rejects.toThrowError(
      new Error("Notify error occurred"),
    );
  });

  it("should throw an error if xerox notification throws an error", async () => {
    // given
    sqsRecord.messageAttributes.deliveryType.stringValue = "letter";
    sendLetterXeroxNotificationSpy = jest
      .spyOn(xeroxNotification, "sendLetterXeroxNotification")
      .mockRejectedValue(new Error("Notify error occurred"));

    // when
    // then
    await expect(processRecord(sqsRecord)).rejects.toThrowError(
      new Error("Notify error occurred"),
    );
  });

  it("should not process the sqs record and throw an error if citizen id is missing", async () => {
    // given
    mockCitizenRecord.id = "";
    getCitizenByIdSpy = jest
      .spyOn(citizen, "getCitizenById")
      .mockResolvedValue(mockCitizenRecord);

    // when
    // then
    await expect(processRecord(sqsRecord)).rejects.toThrowError(
      new Error(
        "Notification has not been created for [citizenId: " +
          mockCertificateRecord.citizenId +
          " and certificateId : " +
          mockCertificateRecord.id +
          "]",
      ),
    );
  });
});
