import {
  loggerWithContext,
  setCorrelationId,
} from "@nhsbsa/hrt-ppc-npm-logging";
import { SQSMessageAttribute, SQSRecord } from "aws-lambda";
import {
  CertificateResponse,
  getCertificateById,
  getCitizenById,
  GovNotifyResponse,
  parseEventType,
  SEND_NOTIFY_EVENT,
  updateNotification,
  XeroxResponse,
} from "./api-utils";
import { Channel } from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";
import {
  Address,
  buildPersonalisationAddress,
  CertificateType,
  CitizenResponse,
  DeliveryType,
  Email,
  EventType,
  MessageBody,
  XeroxNotificationResponse,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { triggerEmailNotification } from "./api-utils/email-notify";
import moment from "moment";
import { isEmpty } from "ramda";
import {
  Personalisation,
  sendLetterXeroxNotification,
} from "./api-utils/xerox-notification";
import {
  castPersonalisationAddressToGovNotifyLetterAddress,
  triggerLetterNotification,
} from "./api-utils/letter-notify";

let logger = loggerWithContext();

export async function processRecord(record: SQSRecord) {
  logger.info(`Processing event [messageId: ${record.messageId}]`);

  const correlationId: string = validMessageAttributes(record);
  const messageBody: MessageBody = validateRecordBody(record);
  const headers = buildHeadersWithCorrelationId(correlationId);
  const notificationId: string | undefined = messageBody.notification?.id;
  const certificateId: string | undefined = messageBody.certificate?.id;
  const { eventType } = record.messageAttributes;
  const eventTypeEnum: EventType | undefined = parseEventType(eventType);
  const { deliveryType } = record.messageAttributes;

  const certificateResponse: CertificateResponse = await getCertificateById(
    certificateId,
    headers,
  );

  const citizenId = certificateResponse.citizenId;

  const citizenResponse: CitizenResponse = await getCitizenById(
    citizenId,
    headers,
  );

  if (!citizenResponse.id || notificationId == null) {
    const message = `Notification has not been created for [citizenId: ${citizenId} and certificateId : ${certificateId}]`;
    logger.error(message);
    throw new Error(message);
  }

  await sendNotification(
    eventTypeEnum,
    correlationId,
    deliveryType,
    citizenResponse,
    certificateResponse,
    notificationId,
  );
}

async function sendNotification(
  eventType: EventType | undefined,
  correlationId: string,
  deliveryType: SQSMessageAttribute,
  citizenResponse: CitizenResponse,
  certificateResponse,
  notificationId,
) {
  const { emails, addresses } = citizenResponse;

  if (deliveryType.stringValue === DeliveryType.email.toString()) {
    if (emails === undefined || isEmpty(emails)) {
      const message = `Preference was ${deliveryType.stringValue} but citizen email missing`;
      throw new Error(message);
    }
    // Call to Gov.uk Notify Service
    const firstEmail = emails[0];
    await sendEmail(
      eventType,
      firstEmail,
      citizenResponse,
      certificateResponse,
      correlationId,
      notificationId,
    );
  }

  if (deliveryType.stringValue === DeliveryType.letter.toString()) {
    const firstAddress = addresses[0];
    if (eventType === EventType.renewal.toString()) {
      // Call to Gov Notify Service
      await sendLetterViaGovNotify(
        eventType,
        firstAddress,
        citizenResponse,
        certificateResponse,
        correlationId,
        notificationId,
      );
    } else {
      // Call to Xerox Notify Service
      await sendLetterViaXerox(
        correlationId,
        firstAddress,
        citizenResponse,
        certificateResponse,
        notificationId,
      );
    }
  }
}

async function sendEmail(
  eventType: EventType | undefined,
  email: Email,
  citizen: CitizenResponse,
  certificate: CertificateResponse,
  correlationId: string,
  notificationId: string,
) {
  const { emailAddress } = email;
  const { id: citizenId, firstName, lastName } = citizen;
  const { id: certificateId, reference, startDate, endDate } = certificate;
  const personalisation = {
    certReference: reference,
    fullName: `${firstName} ${lastName}`,
    startDate: moment(startDate).format("DD MMMM YYYY"),
    endDate: moment(endDate).format("DD MMMM YYYY"),
  };

  let govNotifyResponse: GovNotifyResponse = {
    data: { id: undefined },
  };
  await triggerEmailNotification(personalisation, emailAddress, eventType)
    .then(async (response) => {
      logger.info(
        `Email [notification id: ${notificationId}] created successfully for [citizenId: ${citizenId} & certificate id: ${certificateId} ]`,
      );
      govNotifyResponse = response;
    })
    .catch((error) => {
      const message = `Email notification has not been created for [citizenId: ${citizenId} & certificate id: ${certificateId}] with error: ${error.message}`;
      logger.error(message);
      throw error;
    })
    .finally(async () => {
      await updateNotification(
        buildHeadersWithCorrelationId(correlationId),
        notificationId,
        govNotifyResponse,
      );
    });
}

async function sendLetterViaGovNotify(
  eventType: EventType,
  address: Address,
  citizen: CitizenResponse,
  certificate: CertificateResponse,
  correlationId: string,
  notificationId: string,
) {
  const { id: citizenId, firstName, lastName } = citizen;
  const { id: certificateId, reference, startDate, endDate } = certificate;
  const personalisation = {
    ...castPersonalisationAddressToGovNotifyLetterAddress(
      buildPersonalisationAddress(firstName, lastName)(address),
    ),
    certReference: reference,
    fullName: `${firstName} ${lastName}`,
    startDate: moment(startDate).format("DD MMMM YYYY"),
    endDate: moment(endDate).format("DD MMMM YYYY"),
  };

  let govNotifyResponse: GovNotifyResponse = {
    data: { id: undefined },
  };

  await triggerLetterNotification(personalisation, eventType)
    .then(async (response) => {
      logger.info(
        `Letter [notification id: ${notificationId}] created successfully for [citizenId: ${citizenId} & certificate id: ${certificateId} ]`,
      );
      govNotifyResponse = response;
    })
    .catch((error) => {
      const message = `Letter notification has not been created for [citizenId: ${citizenId} & certificate id: ${certificateId}] with error: ${error.message}`;
      logger.error(message);
      throw error;
    })
    .finally(async () => {
      await updateNotification(
        buildHeadersWithCorrelationId(correlationId),
        notificationId,
        govNotifyResponse,
      );
    });
}

async function sendLetterViaXerox(
  correlationId: string,
  address: Address,
  citizen: CitizenResponse,
  certificate,
  notificationId: string,
) {
  const { id: citizenId, firstName, lastName } = citizen;
  const { id: certificateId, reference, startDate, endDate } = certificate;
  const personalisation: Personalisation = {
    certificateReference: reference,
    certificateStartDate: moment(startDate).format("DDMMYYYY"),
    certificateEndDate: moment(endDate).format("DDMMYYYY"),
    firstName,
    lastName,
    ...buildPersonalisationAddress(firstName, lastName)(address),
  };
  let xeroxResponse: XeroxResponse = {};
  await sendLetterXeroxNotification(correlationId, reference, personalisation)
    .then(async (response: XeroxNotificationResponse) => {
      logger.info(
        `Letter [notification id: ${notificationId}] created successfully for [citizenId: ${citizenId} & certificate id: ${certificateId} ]`,
      );
      xeroxResponse = response;
    })
    .catch((error) => {
      const message = `Letter notification has not been created for [citizenId: ${citizenId} & certificate id: ${certificateId}] with error: ${error.message}`;
      logger.error(message);
      xeroxResponse.status = error.statusCode;
      throw error;
    })
    .finally(async () => {
      if (xeroxResponse?.status === 409) {
        return;
      }
      await updateNotification(
        buildHeadersWithCorrelationId(correlationId),
        notificationId,
        xeroxResponse,
      );
    });
}

function validMessageAttributes(record: SQSRecord) {
  logger.info(`Validating SQS message attributes`);

  const {
    eventName,
    source,
    certificateType,
    eventType,
    deliveryType,
    correlationId,
  } = record.messageAttributes;
  const eventNameValue = eventName?.stringValue;
  const sourceValue = source?.stringValue;
  const certificateTypeValue = certificateType?.stringValue;
  const eventTypeValue = eventType?.stringValue;
  const deliveryTypeValue = deliveryType?.stringValue;
  const correlationIdValue = correlationId?.stringValue;

  logger.info(
    `Received [eventName: ${eventNameValue}, source: ${sourceValue}, certificateType: ${certificateTypeValue}, eventType: ${eventTypeValue}, deliveryType: ${deliveryTypeValue}, correlationId: ${correlationIdValue}]`,
  );

  if (
    eventTypeValue !== EventType.issueCertificate.toString() &&
    eventTypeValue !== EventType.reissueCertificate.toString() &&
    eventTypeValue !== EventType.renewal.toString()
  ) {
    const message =
      "Invalid SQS message attributes, event type can only be " +
      EventType.issueCertificate.toString() +
      " or " +
      EventType.reissueCertificate.toString() +
      " or " +
      EventType.renewal.toString();
    logger.warn(message);
    throw new Error(message);
  }

  if (certificateTypeValue !== CertificateType.HRT_PPC.toString()) {
    const message =
      "Invalid SQS message attributes, certificate type can only be " +
      CertificateType.HRT_PPC.toString();
    logger.warn(message);
    throw new Error(message);
  }

  if (
    deliveryTypeValue !== DeliveryType.email.toString() &&
    deliveryTypeValue !== DeliveryType.letter.toString()
  ) {
    const message =
      "Invalid SQS message attributes, delivery type can only be " +
      DeliveryType.email.toString() +
      " or " +
      DeliveryType.letter.toString();
    logger.warn(message);
    throw new Error(message);
  }

  if (!correlationIdValue) {
    const message =
      "Invalid SQS message attributes, must contain correlationId";
    logger.error(message);
    throw new Error(message);
  }
  setCorrelationId(correlationIdValue);
  logger = loggerWithContext();
  return correlationIdValue;
}

function validateRecordBody(record: SQSRecord) {
  logger.info(`Validating record body`);
  let messageBody: MessageBody;
  try {
    messageBody = JSON.parse(record.body);
  } catch (err) {
    const message =
      "Unable to parse record body: " +
      record.body +
      ` with error ${JSON.stringify(err)}`;
    logger.error(message);
    throw new Error(message);
  }

  logger.info(`Received ${JSON.stringify(messageBody)}`);

  if (
    !messageBody.certificate ||
    !messageBody.certificate.id ||
    !messageBody.notification ||
    !messageBody.notification.id
  ) {
    const message = `Invalid record body, must contain notificationId, certificateId`;
    logger.error(message);
    throw new Error(message);
  }

  return messageBody;
}

function buildHeadersWithCorrelationId(correlationId: string) {
  return {
    channel: Channel.BATCH,
    "user-id": SEND_NOTIFY_EVENT,
    "correlation-id": correlationId,
  };
}
