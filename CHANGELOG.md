# # Hrt PPC notification outbound Lambda Changelog

## v0.0.0 - 24 Oct 2023

:new: **New features**

- {feature}

:wrench: **Fixes**

- {issue}
