# hrt-ppc-notification-outbound-lambda

## Description

This lambda is to generate Notification based on creation of notification record.

## Getting started

### System requirements

The application requires Node JS, NPM package manager to be installed.

[Installing Node JS and NPM ](https://nodejs.org/en/download/)

## Install dependencies

To install the project dependencies run the following command from the project root:

```
npm install
```

Install the latest release of (AWS SAM CLI) to support with node version 18.x
[Installing the AWS SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/install-sam-cli.html)

SAM would pull the runtime image from docker to execute the lambda code locally  
[Get Docker](https://docs.docker.com/get-docker/)

## Secrets manager setup
The application depends on retrieving secrets from secrets manager, clone [local-cache-secrets-manager-mock](https://gitlab.com/nhsbsa/citizen-services/exemptions/hrt-ppc/local-cache-secrets-manager-mock). The following secrets will be retrieved

| Key                                   | Description                                       |
|---------------------------------------|---------------------------------------------------|
| CERTIFICATE_API_BASE_URL              | Base url for certificate service                  |
| CERTIFICATE_API_KEY                   | Api key for certificate service                   |
| CITIZEN_API_BASE_URL                  | Base url for citizen service                      |
| CITIZEN_API_KEY                       | Api key for citizen service                       |
| GOV_NOTIFY_API_KEY                    | Api key for gov notify service                    |
| GOV_NOTIFY_EMAIL_ISSUE_TEMPLATE_ID    | Email Issue template id for gov notify service    |
| GOV_NOTIFY_EMAIL_REISSUE_TEMPLATE_ID  | Email Reissue template id for gov notify service  |
| GOV_NOTIFY_EMAIL_RENEWAL_TEMPLATE_ID  | Email Renewal template id for gov notify service  |
| GOV_NOTIFY_LETTER_RENEWAL_TEMPLATE_ID | Letter Renewal template id for gov notify service |
| XEROX_API_KEY                         | Api key for xerox service                         |
| XEROX_API_BASE_URL                    | Base url for xerox service                        |
| XEROX_NOTIFICATION_TEMPLATE_ID        | Letter template id for xerox notification service |
| NOTIFICATION_API_BASE_URL             | Base url for notification service                 |
| NOTIFICATION_API_KEY                  | Api key for notification service                  |

## Environment Variables

| Key                         | Value                                  | Description                       |
| ------------------------    | --------------------------------       | --------------------------------- |
| outBoundApiTimeout          | 30000                                  | Timeout for outbound api requests |

## Optional Environment Variables for Dev and Test

| Key                                         | Value                                 | Description                                                                            |
|---------------------------------------------|---------------------------------------|----------------------------------------------------------------------------------------|
| logApiRequestAndResponse                    | enable                                | Any string value to enable this feature                                                |
| PARAMETERS_SECRETS_EXTENSION_HTTP_HOST      | localhost                             | The secret manager host                                                                |
| CERTIFICATE_API_BASE_URL_SM_ID              | CERTIFICATE_API_BASE_URL              | The secret name to retrieve url from secrets manager                                   |
| CERTIFICATE_API_KEY_SM_ID                   | CERTIFICATE_API_KEY                   | The secret name to retrieve key from secrets manager                                   |
| CITIZEN_API_BASE_URL_SM_ID                  | CITIZEN_API_BASE_URL                  | The secret name to retrieve url from secrets manager                                   |
| CITIZEN_API_KEY_SM_ID                       | CITIZEN_API_KEY                       | The secret name to retrieve key from secrets manager                                   |
| GOV_NOTIFY_API_KEY_SM_ID                    | GOV_NOTIFY_API_KEY                    | The secret name to retrieve key from secrets manager                                   |
| GOV_NOTIFY_EMAIL_ISSUE_TEMPLATE_ID_SM_ID    | GOV_NOTIFY_EMAIL_ISSUE_TEMPLATE_ID    | The secret name to retrieve gov notify issue email template id from secrets manager    |
| GOV_NOTIFY_EMAIL_REISSUE_TEMPLATE_ID_SM_ID  | GOV_NOTIFY_EMAIL_REISSUE_TEMPLATE_ID  | The secret name to retrieve gov notify reissue email template id from secrets manager  |
| GOV_NOTIFY_EMAIL_RENEWAL_TEMPLATE_ID_SM_ID  | GOV_NOTIFY_EMAIL_RENEWAL_TEMPLATE_ID  | The secret name to retrieve gov notify renewal email template id from secrets manager  |
| GOV_NOTIFY_LETTER_RENEWAL_TEMPLATE_ID_SM_ID | GOV_NOTIFY_LETTER_RENEWAL_TEMPLATE_ID | The secret name to retrieve gov notify renewal letter template id from secrets manager |
| XEROX_API_KEY_SM_ID                         | XEROX_API_KEY                         | The secret name to retrieve key from secrets manager                                   |
| XEROX_API_BASE_URL_SM_ID                    | XEROX_API_BASE_URL                    | The secret name to retrieve url from secrets manager                                   |
| XEROX_NOTIFICATION_TEMPLATE_ID_SM_ID        | XEROX_NOTIFICATION_TEMPLATE_ID        | The secret name to retrieve xerox notification template id from secrets manager        |
| NOTIFICATION_API_BASE_URL_SM_ID             | NOTIFICATION_API_BASE_URL             | The secret name to retrieve url from secrets manager                                   |
| NOTIFICATION_API_KEY_SM_ID                  | NOTIFICATION_API_KEY                  | The secret name to retrieve key from secrets manager                                   |

To use this lambda locally, You would need to create an **env.json** file
see [example](./events/sample-env.json)

## Usage

There are two ways can invoke this lambda locally,
This consumes a static SQS event. See [example](./events/event-single-record.json)

1. SAM CLI

```
npm run start
```

2. Quick Invoke Extension
   [See More here](https://marketplace.visualstudio.com/items?itemName=bogdan-onu.invoke)

3. Hosts file
   Check the hosts file has hostname mapping for **host.docker.internal**.

```
# Added by Docker Desktop
127.0.0.1 host.docker.internal
```

You cloud have this hostname already, added by Docker Desktop.
If not, add it manually to the file with the administrative right.

- Windows `C:\Windows\System32\drivers\etc\hosts`
- Mac `/private/etc/hosts`

## Mock API Endpoint

As this lambda expects a SQS message event,
If you want to call this lambda from an api endpoint,
It would need this lambda to be deployed to the localstack.

## Deploying as a lambda to localstack

There is a shell script that packages everything up and deploys as a .zip to hrt-ppc-localstack

1. Make sure you have cloned the repo for hrt-ppc-localstack
2. Run [./deploy-localstack.sh](./deploy-localstack.sh)
3. Execute 'start.sh' and 'setup.sh' in the localstack repo which will make this lambda executable over api-gateway
